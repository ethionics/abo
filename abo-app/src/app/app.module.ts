import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {FormsModule} from '@angular/forms';
import {HttpModule} from '@angular/http';
import {
  RouterModule,
  Routes
} from '@angular/router';

import {
  routes as eventsChildRoutes,
  EventsModule
} from './events/events.module';

import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { GalleryComponent } from './gallery/gallery.component';
import { LoginComponent } from './login/login.component';
import { ContactComponent } from './contact/contact.component';
import { EventsComponent } from './events/events.component';
import { ProtectedComponent } from './protected/protected.component';

import {AUTH_PROVIDERS} from './auth.service';
import {LoggedInGuard} from './logged-in.guard';
import { NavbarComponent } from './navbar/navbar.component';
import { FooterComponent } from './footer/footer.component';
import { EventComponent } from './events/event/event.component';

const routes: Routes = [
  {path: '', redirectTo: 'home', pathMatch: 'full'},
  {path: 'home', component: HomeComponent},
  // nested
  {
    path: 'events',
    component: EventsComponent,
    children: eventsChildRoutes,
  },
  {path: 'gallery', component: GalleryComponent},
  {path: 'contact', component: ContactComponent},

  // authentication
  {path: 'login', component: LoginComponent},
  {
    path: 'protected',
    component: ProtectedComponent,
    canActivate: [LoggedInGuard]
  }

  // TODO: Check usage of child routes
  ];

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    GalleryComponent,
    LoginComponent,
    ContactComponent,
    EventsComponent,
    ProtectedComponent,
    NavbarComponent,
    FooterComponent,
    EventComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes),

    // added this for our child module
    EventsModule
  ],
  providers: [
    // uncomment this for "hash-bang" routing
    // { provide: LocationStrategy, useClass: HashLocationStrategy }
     AUTH_PROVIDERS,
     LoggedInGuard
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
