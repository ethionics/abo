/*
 * events.module.ts
 *
 * Created on 2018-04-30
 *
 * Copyright (C) 2018 Volkswagen AG, All rights reserved.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {
  RouterModule, ActivatedRoute, Router, Routes
} from '@angular/router';

import {EditEventsComponent} from './edit-events/edit-events.component';
import {LoggedInGuard} from '../logged-in.guard';
import {EventsService} from "./events.service";

export const routes: Routes = [
  /*{path: '', redirectTo: 'main', pathMatch: 'full'},*/
  {
    path: 'edit',
    component: EditEventsComponent,
    canActivate: [LoggedInGuard]
  },
];

@NgModule({
  declarations: [
    EditEventsComponent
  ],
  exports: [
    EditEventsComponent
  ],
  imports: [
    CommonModule, RouterModule
  ],
  providers: [
    EventsService
  ]
})
export class EventsModule {
}
