import {Injectable} from "@angular/core";
import {Subject, BehaviorSubject, Observable} from "rxjs";
import { Event } from './event/event.model';

const initialEvents: Event[] = [
  new Event(new Date(), 'First Event', 'Description of first event, Description of first event'
    + 'Description of first event'),
  new Event(new Date(),'Second Event', 'Description of second event')
];


interface IEventsOperation extends Function {
  (events: Event[]): Event[];
}

@Injectable()
export class EventsService {
  // A stream that emits an array of events. Observable<Message[]> is same as Event[]: it means this stream emits array not inidividual Event
  // Use 'The Opperation Stream pattern' to populate 'events' using 'update' stream
  events: Observable<Event[]>;

  // `updates` receives 'operations' to be applied to our `events`
  updates: Subject<any> = new Subject<any>();

  constructor() {
    // TODO: Try page 289
    this.events = this.updates
    // watch the updates and accumulate operations on the messages
      .scan((events: Event[],
             operation: IEventsOperation) => {
          console.log(`Scan content: ` + this.events);

          return operation(events);
        },
        initialEvents)
      // make sure we can share the most recent list of events across anyone
      // who's interested in subscribing and cache the last known list of
      // events
      .publishReplay(1)
      .refCount();

    console.log(`Service content: ` + this.events);
  }

  ngOnInit() {
  }

    // TODO: Use HTTP service to load initial list of events, and add each to 'events'
  loadEvents(): Observable<Event[]>{
    // TODO: Check if this should be returned directly
    return this.events;
  }

  addEvent(newEvent: Event): void{
    this.updates.next((events: Event[]): Event[] => {
      return events.concat(newEvent);
    })
  }

  removeEvent(event: Event): void{
    this.updates.next((events: Event[]): Event[] => {
      // TODO: add remove from array functionality
      return events;
    })
  }
}
