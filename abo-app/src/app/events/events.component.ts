import { Component, OnInit } from '@angular/core';
import { Event } from './event/event.model';
import {EventsService} from "./events.service";

@Component({
  selector: 'app-events',
  templateUrl: './events.component.html',
  styleUrls: ['./events.component.css']
})
export class EventsComponent implements OnInit {
  events: Event[];

  constructor(private eventsService: EventsService) {
  }

  ngOnInit() {
    this.eventsService.loadEvents().subscribe(
      (events: Event[]) => {
        console.log(`Subscribe in events called`);
        this.events = events}
    )

    // TODO: Find a way to trigger subscribe without adding initial data
   /*this.eventsService.addEvent(new Event(new Date(), 'Trigger Events',
      'Remove this event when solution is found'));*/
  }

  sortedEvents(): Event[] {
    return this.events;
    // TODO: Sort by date
    // return this.events.sort((a: Event, b: Event) => b.date.getDate() - a.date.getDate());
  }
}
