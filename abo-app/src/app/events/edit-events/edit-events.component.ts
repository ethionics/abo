import { Component, OnInit } from '@angular/core';
import {EventsService} from "../events.service";
import {Event} from "../event/event.model";

@Component({
  selector: 'app-edit-events',
  templateUrl: './edit-events.component.html',
  styleUrls: ['./edit-events.component.css']
})
export class EditEventsComponent implements OnInit {

  constructor(private eventsService: EventsService) { }

  ngOnInit() {
  }

  addEvent(date: Date, title: HTMLInputElement, description: HTMLInputElement): boolean {
    console.log(`Adding EVENT title: ${title.value}, description: ${description.value}`);
    this.eventsService.addEvent(new Event(new Date(), title.value,description.value));
    title.value = '';
    description.value = '';
    return false;
  }

}
