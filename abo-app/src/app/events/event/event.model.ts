/*
 * event.model.ts
 *
 * Created on 2018-04-30
 *
 * Copyright (C) 2018 Volkswagen AG, All rights reserved.
 */
export class Event {
  // TODO: Use this Datepicker: https://ng-bootstrap.github.io/#/components/datepicker/examples
  date: Date;
  title: string;
  description: string;

  constructor(date: Date, title: string, description: string) {
    this.date = date;
    this.title = title;
    this.description = description;
  }
}
