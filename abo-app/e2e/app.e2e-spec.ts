import { AboAppPage } from './app.po';

describe('abo-app App', () => {
  let page: AboAppPage;

  beforeEach(() => {
    page = new AboAppPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!');
  });
});
