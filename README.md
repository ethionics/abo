# abo

# Start Project
- go to folder "/abo-app"
- enter "npm install"
- enter "ng serve"
- In browser open "http://localhost:4200"

# Enter events
## Login
- open url "http://localhost:4200/login"
- username: admin password: admin

## Add Event
- After login, open "http://localhost:4200/events/edit"


# Issues and fixes

- Not accessible localy with ip/only on localhost
fix: ng serve --host 0.0.0.0

- Invalid Host Header
fix1: ng serve --host 0.0.0.0 --public 192.168.0.39
fix2: https://stackoverflow.com/questions/43677629/invalid-host-header-when-running-angular-cli-development-server-c9-io

- Collapse Hamburger Menu on click
fix: Add collapse component to li(*not to <a>; it will cause routerLink issues) -> <li class="nav-item" data-toggle="collapse" data-target=".navbar-collapse.show">
https://stackoverflow.com/questions/42401606/how-to-hide-collapsible-bootstrap-4-navbar-on-click



